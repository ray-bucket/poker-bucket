defmodule PokerBucketWeb.RoomLive.New do
  alias PokerBucket.Rooms.Room
  alias PokerBucket.Repo

  use PokerBucketWeb, :live_view
  use PokerBucketWeb, :controller

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def handle_event("create", %{"room" => %{"name" => name}}, socket) do
    changeset = Room.changeset(%Room{}, %{name: name})

    case Repo.insert(changeset) do
      {:ok, room} ->
        {:noreply, push_redirect(socket, to: Routes.room_show_path(socket, :show, room.id))}
      {:error, changeset} ->
        {:noreply, assign(socket, error_message: "Error creating room")}
    end
  end
end
