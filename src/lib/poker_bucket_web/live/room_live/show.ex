defmodule PokerBucketWeb.RoomLive.Show do
  alias PokerBucket.Rooms.Room
  alias PokerBucket.Repo

  use PokerBucketWeb, :live_view

  def mount(%{"id" => id}, _session, socket) do
    room = Repo.get(Room, id)

    {:ok, assign(socket, room: room)}
  end
end
