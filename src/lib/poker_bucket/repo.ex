defmodule PokerBucket.Repo do
  use Ecto.Repo,
    otp_app: :poker_bucket,
    adapter: Ecto.Adapters.Postgres
end
